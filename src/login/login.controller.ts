import { Body, Controller, Get, HttpCode, HttpException, HttpStatus, Post } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { LoginDto } from './login.dto';
const bcrypt = require('bcrypt');

@Controller('login')
export class LoginController {
  constructor (
    private usersService: UsersService,
    private jwtService: JwtService) { }

  @HttpCode(200)
  @Post()
  async create(@Body() dto: LoginDto) {
    const rs: any = await this.usersService.getHashPassword(dto.username);
    if (rs) {
      if (bcrypt.compareSync(dto.password, rs.password)) {
        const payload = {
          sub: rs.userId,
          iss: 'http://sso.r7platform.com'
        };
        const access_token = this.jwtService.sign(payload);
        return { access_token }
      } else {
        throw new HttpException('รหัสผ่านไม่ถูกต้อง', HttpStatus.NOT_FOUND);
      }
    } else {
      throw new HttpException('ไม่พบอีเมล์ในระบบ', HttpStatus.NOT_FOUND);
    }
  }

  @HttpCode(200)
  @Post('/generate')
  async createHash(@Body('password') password: string) {
    const saltRounds = 12;
    const salt = bcrypt.genSaltSync(saltRounds);
    const hash = bcrypt.hashSync(password, salt);

    return { hash }
  }

}

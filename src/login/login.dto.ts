import { Length, IsString, IsISO8601, IsOptional, IsNumber, IsNumberString, IsEmail } from 'class-validator';


export class LoginDto {

  @IsString()
  username: string;

  @IsString()
  password: string;

}

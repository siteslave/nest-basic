import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { MulterModule } from '@nestjs/platform-express';
import { AppModule } from './app.module';

async function bootstrap() {

  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({
      disableErrorMessages: false,
    }),
  );

  MulterModule.register({
    dest: './upload',
  });

  app.enableCors();
  await app.listen(3000, '0.0.0.0');
}

bootstrap();

import { Controller, Get, Header, Param, Query, Res, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { EmployeesService } from '../employees/employees.service';
import { LibsService } from './libs.service';
import * as path from 'path';
import * as fs from 'fs';
@UseGuards(JwtAuthGuard)
@Controller('libs')
export class LibsController {

  constructor (
    private libService: LibsService,
    private readonly employeesService: EmployeesService) { }

  @Get('positions')
  getPositions() {
    return this.libService.getPositions();
  }

  @Get('departments')
  getDepartments() {
    return this.libService.getDepartment();
  }

  @Get('image/profile/:id')
  @Header('Content-Type', 'image/png')
  @Header('Content-Disposition', 'attachment; filename=profile.png')
  async getImageProfile(@Param('id') id: number, @Res() res: any) {
    let employee = await this.employeesService.findOne(id);
    if (employee) {
      let profileImage;
      if (employee.imagePath != null) {
        let imagePath = path.join('./uploads', employee.imagePath);
        if (fs.existsSync(imagePath)) {
          profileImage = imagePath;
        } else {
          profileImage = path.join(__dirname, '../../public/images/placeholder.png')
        }
      } else {
        profileImage = path.join(__dirname, '../../public/images/placeholder.png')
      }

      return fs.createReadStream(profileImage).pipe(res);
    } else {
      let profileImage = path.join(__dirname, '../../public/images/placeholder.png')
      return fs.createReadStream(profileImage).pipe(res);
    }

  }
}

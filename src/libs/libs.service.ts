import { Injectable } from '@nestjs/common';
import { Connection, EntityManager } from 'typeorm';

@Injectable()
export class LibsService {

  entityManager: EntityManager;

  constructor (
    private readonly connection: Connection
  ) {
    this.entityManager = this.connection.createEntityManager();
  }

  getPositions(): Promise<any[]> {
    return this.entityManager.query(`
      select * from positions
      order by position_name
    `)
  }

  getDepartment(): Promise<any[]> {
    return this.entityManager.query(`
      select * from departments
      order by department_name
    `)
  }
}

import { Length, IsString, IsISO8601, IsOptional, IsNumber, IsNumberString, IsEmail } from 'class-validator';

export class UpdateEmployeeDto {
  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsString()
  birthdate: string;

  @IsString()
  sex: string;

  @IsNumberString()
  departmentId: number;

  @IsString()
  @IsOptional()
  imagePath: string;

  @IsNumberString()
  positionId: number;

  @IsNumberString()
  @IsOptional()
  lat: number;

  @IsNumberString()
  @IsOptional()
  lng: number;
}

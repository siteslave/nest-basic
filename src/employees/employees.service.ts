import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, EntityManager, Repository } from 'typeorm';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';

@Injectable()
export class EmployeesService {

  entityManager: EntityManager;

  constructor (
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    private readonly connection: Connection,
  ) {
    this.entityManager = this.connection.createEntityManager();
  }

  create(employee: Employee) {
    return this.employeeRepository.save(employee);
  }

  findByDepartment(departmentId: number) {
    return this.employeeRepository.find({
      where: {
        departmentId
      }
    })
  }

  findAll() {
    return this.entityManager.query(`
    SELECT e.*, d.department_name, p.position_name
    FROM employees as e
    LEFT JOIN departments AS d ON d.department_id=e.department_id
    LEFT JOIN positions AS p ON p.position_id=e.position_id
    `);
  }

  findOne(id: number) {
    return this.employeeRepository.findOne(id);
  }

  update(id: number, dto: UpdateEmployeeDto) {
    return this.employeeRepository.update({ employeeId: id }, {
      firstName: dto.firstName,
      lastName: dto.lastName,
      positionId: dto.positionId,
      birthdate: dto.birthdate,
      imagePath: dto.imagePath,
      lat: dto.lat,
      lng: dto.lng,
      departmentId: dto.departmentId
    });
  }

  updateImage(id: number, imagePath: any) {
    return this.employeeRepository.update({ employeeId: id }, {
      imagePath: imagePath
    });
  }

  remove(id: number) {
    return this.employeeRepository.delete(id);
  }
}

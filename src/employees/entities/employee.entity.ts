import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'employees' })
export class Employee {
  @PrimaryGeneratedColumn({ name: 'employee_id' })
  employeeId: number;

  @Column({ name: 'first_name' })
  firstName: string;

  @Column({ name: 'last_name' })
  lastName: string;

  @Column({ name: 'birthdate' })
  birthdate: string;

  @Column({ name: 'sex' })
  sex: string;

  @Column({ name: 'department_id' })
  departmentId: number;

  @Column({ name: 'image_path' })
  imagePath: string;

  @Column({ name: 'position_id' })
  positionId: number;

  @Column({ name: 'lat' })
  lat: number;

  @Column({ name: 'lng' })
  lng: number;

}

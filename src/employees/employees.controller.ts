import { Controller, Get, Post, Body, Put, Param, Delete, HttpStatus, HttpCode, UseGuards, Req, UseInterceptors, UploadedFile } from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import * as multer from 'multer';
import * as path from 'path';
@UseGuards(JwtAuthGuard)
@Controller('employees')
export class EmployeesController {


  constructor (private readonly employeesService: EmployeesService) { }

  @HttpCode(201)
  @Post()
  async create(@Body() dto: CreateEmployeeDto) {
    let employee: Employee = new Employee();
    employee.firstName = dto.firstName;
    employee.lastName = dto.lastName;
    employee.birthdate = dto.birthdate;
    employee.positionId = dto.positionId;
    employee.imagePath = dto.imagePath;
    employee.departmentId = dto.departmentId;
    employee.sex = dto.sex;
    employee.lat = dto.lat;
    employee.lng = dto.lng;

    await this.employeesService.create(employee);
    return;
  }

  @Get()
  findAll() {
    return this.employeesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.employeesService.findOne(+id);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() dto: UpdateEmployeeDto) {
    await this.employeesService.update(+id, dto);
    return;
  }

  @Post(':id/upload')
  @UseInterceptors(FileInterceptor('file', {
    storage: multer.diskStorage({
      destination: function (req: any, file: any, cb: any) {
        cb(null, './uploads')
      },
      filename: function (req: any, file: any, cb: any) {
        const _ext = path.extname(file.originalname)
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, file.fieldname + '-' + uniqueSuffix + _ext)
      }
    })
  }))
  async updateImage(@Param('id') id: number, @UploadedFile() file: any) {
    console.log(file);
    await this.employeesService.updateImage(id, file.filename);
    return file;
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    await this.employeesService.remove(+id);
    return;
  }
}

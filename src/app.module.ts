import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { LoginController } from './login/login.controller';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { jwtConstants } from './auth/constants';
import { JwtStrategy } from './auth/jwt.strategy';
import { LibsController } from './libs/libs.controller';
import { LibsService } from './libs/libs.service';
import { EmployeesModule } from './employees/employees.module';

import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    ConfigModule.forRoot(
      {
        envFilePath: '.development.env',
      }
    ),
    TypeOrmModule.forRoot(
      {
        "type": "mysql",
        "host": process.env.DATABASE_HOST,
        "port": parseInt(process.env.PORT, 10) || 3306,
        "username": process.env.DATABASE_USER,
        "password": process.env.DATABASE_PASSWORD,
        "database": process.env.DATABASE_NAME,
        "entities": [
          "dist/**/*.entity{.ts,.js}"
        ],
        "autoLoadEntities": true,
        "synchronize": false
      }
    ),
    UsersModule,
    AuthModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1d' },
    }),
    EmployeesModule,
  ],
  controllers: [AppController, LoginController, LibsController],
  providers: [AppService, JwtStrategy, LibsService],
})
export class AppModule { }
